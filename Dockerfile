FROM openjdk:8
COPY src/main/resources/application.yml /root/.m2/repository/com/mindzen/infra/
COPY plugin/ /root/.m2/repository/com/mindzen/
COPY src/main/resources/application.yml $HOME/
COPY src/main/resources/RWOperations.txt /home/mindzen/RWOperations.java
COPY src/main/resources/trigger.sh /home/mindzen/trigger.sh
COPY build/libs/discovery-server-0.0.1-SNAPSHOT.jar discovery-server.jar
EXPOSE 9901
ENTRYPOINT ["java" , "-jar", "discovery-server.jar"]
